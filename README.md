This simple little Bash script, in which the user sets his preferred programs,
allows for maximum laziness in accessing of visual media from the command line.
To be precise, it takes an argument and checks the extension of the given file;
using that extension, it then calls the appropriate program for that type of
extension, presuming it is supported.  

By default, the script calls a number of different programs to view files with
the following extensions:
    Image:	png, jpg, jpeg, jpe, gif, bmp
	Video:	mp4, avi, mkv, ogv, wmv, mpg, webm
	Doc:	doc, docx, xls, odf, ods, odt
	Other:	pdf, txt

It's expected that the user will be editing the script to his own tastes, both
to assign default programs and to extend the number of supported extensions.
It would be trivially easy to add mp3 support, for instance, but I've simply
chosen not to to keep with the "visual" theme.
