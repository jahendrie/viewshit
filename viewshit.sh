#!/bin/bash
#===============================================================================
#   viewshit.sh     |   version 1.0     |   MIT License     |   2017.02.21
#   James Hendrie   |   hendrie.james@gmail.com
#
#   This script provides for a quick and easy way to view a number of different
#   types of file with one handy little command.  Indeed, I am too lazy to
#   bother invoking more than a few commands for my most commonly-accessed
#   files.  You can alias this to something even shorter, such as simply 'v',
#   for optimal laziness.
#
#   Obviously, you're going to need to choose which programs you want for the
#   various different types of media you'll be viewing.  I've set up my own
#   personal defaults just below, change them as you see fit.
#===============================================================================

##  Program to play our video files
VIDEO_PLAYER="$(which mpv)"

##  Program to view our image files
IMAGE_VIEWER="$(which viewnior)"

##  Program to view the PDF files
PDF_VIEWER="$(which atril)"

##  Program to view or edit document files
DOC_EDITOR="$(which libreoffice)"

##  Text viewer (pager)
TEXT_VIEWER="$(which less)"




##  Prints the usage
function print_usage
{
    echo "Usage:  viewshit.sh FILE"
}


##  If they didn't provide a file (or ask for help), get ridda da bum
if [[ $# -lt 1 ]]; then
    echo "ERROR:  Too few arguments." 1>&2
    print_usage 1>&2
    exit 1
fi


##  If they ask for help, print it then exit
if [[ "$1" = "-h" ]] || [[ "$1" = "--help" ]]; then
    print_usage
    echo ""
    echo "This script is the lazy man's way to view image, video or text files."
    echo "It works by looking at the extension of the first file passed and"
    echo "calling a program to deal with that extension; all files given as"
    echo "arguments will be processed with that program, correctly or not."
    echo ""
    echo "File extensions that will be tried:"
    echo "  Image:  png, jpg, jpeg, jpe, gif, bmp"
    echo "  Video:  mp4, avi, mkv, ogv, wmv, mpg, webm"
    echo "  Doc:    doc, docx, xls, odf, ods, odt"
    echo "  Other:  pdf, txt"
    echo ""
    echo "Programs called:"
    echo -e "  Image viewer:\t\t${IMAGE_VIEWER}"
    echo -e "  Video player:\t\t${VIDEO_PLAYER}"
    echo -e "  Document editor:\t${DOC_EDITOR}"
    echo -e "  PDF viewer:\t\t${PDF_VIEWER}"
    echo -e "  Text viewer:\t\t${TEXT_VIEWER}"

    exit 0

fi


##  Assign the first arg to filename variable (easier)
filename="$1"

##  Get the extension from the first filename and lowercase it
ext="$(echo "${filename##*.}" | tr '[:upper:]' '[:lower:]')"


##  Run through the extensions we're checking
case "${ext}" in
    ##  Image viewers
    gif) ;&
    png) ;&
    jpg) ;&
    jpeg) ;&
    jpe) ;&
    bmp)
        `${IMAGE_VIEWER} "$@"`
        exit 0 ;;

    ##  PDF
    pdf)
        `${PDF_VIEWER} "$@"`
        exit 0 ;;

    ##  Video
    mp4) ;&
    avi) ;&
    webm) ;&
    wmv) ;&
    mpg) ;&
    mkv) ;&
    ogv)
        `${VIDEO_PLAYER} "$@"`
        exit 0 ;;

    ##  Documents
    doc) ;&
    dot) ;&
    docx) ;&
    xls) ;&
    xlsx) ;&
    ppt) ;&
    pps) ;&
    pptx) ;&
    rtf) ;&
    sdw) ;&
    sgl) ;&
    vor) ;&
    wps) ;&
    wpd) ;&
    ods) ;&
    odf) ;&
    odp) ;&
    odt)
        `${DOC_EDITOR} "$@"`
        exit 0 ;;

    ##  Regular ol' text
    txt)
        `${TEXT_VIEWER} "$@"`
        exit 0 ;;

    ##  Anything else
    *)
        echo "ERROR:  Can't handle filetype with extension '${ext}'" 1>&2
        exit 1 ;;

esac

##  All done
exit 0
